import redis
import json


class StatusRedis(object):
    """docstring for StatusRedis"""
    def __init__(self, arg):
        self.r = redis.Redis(host='status.multifiber.cl',
                 port=6379, db=0)


    def emit(self, data, evento):
        self.r.publish('server-to-clients', json.dumps(data))
from django.db import (models,connection,connections)
from itertools import tee, islice, chain
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from simple_history.models import HistoricalRecords


class BaseModelWithoutHistory(models.Model):
    created = models.DateTimeField(auto_now_add = True, editable=True)
    modified = models.DateTimeField(auto_now = True)
    status_field = models.BooleanField(default = True)
    id_data = models.UUIDField(db_index = True,
                               null = True, blank = True,
                               editable = settings.DEBUG)

    class Meta:
        abstract = True


def previous_and_next(some_iterable):
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)
    
class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add = True, editable=True)
    modified = models.DateTimeField(auto_now = True)
    status_field = models.BooleanField(default = True)
    id_data = models.UUIDField(db_index = True,
                               null = True, blank = True,
                               editable = settings.DEBUG)
    history = HistoricalRecords(inherit = True)

    @classmethod
    def truncate(cls):
        """
        Método de la clase para hacer Truncate sobre los modelos y 
        reiniciar los indices.

        modo de aplicar truncate:
        Model.objects.all().model().truncate()

        """
        TABLE = cls._meta.db_table

        try:
            sql = f'TRUNCATE TABLE {TABLE} RESTART IDENTITY CASCADE;'
            with connection.cursor() as cursor:
                cursor.execute(sql)
        except Exception as e:
            error = str(e)
            print(error)


    def action_log(self, just=[]):
        fields = self.__class__._meta.fields
        filtered_fields = list(filter(lambda f: f.get_attname() in just, fields) if just else fields)
        field_names = [f.get_attname() for f in filtered_fields]
        historic_field_names = (just or field_names) + ['history_user_id', 'history_date']
        users = {None: 'matrix'}
        users.update({u.pk: u.username for u in get_user_model().objects.filter(id__in=self.history.values_list('history_user_id', flat=True).distinct())})
        rx = self.history.values(*historic_field_names)
        changes = []

        
        for prev, record, nxt in previous_and_next(rx):
            if nxt:
                changed_fields = [f for f in filtered_fields if record[f.get_attname()] != nxt[f.get_attname()]]
                verbose_changed_fields = [f.verbose_name for f in changed_fields]
                if changed_fields:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('changed'),
                                    'changes': verbose_changed_fields,
                                    'current_values': {field.attname: record[field.attname] for field in changed_fields},
                                    'previous_values': {field.attname: nxt[field.attname] for field in changed_fields},
                                    'dt': record['history_date']})
            else:
                if not just:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('created'),
                                    'dt': record['history_date']})
        return changes

    class Meta:
        abstract = True


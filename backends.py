from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from rest_framework.authentication import BaseAuthentication, CSRFCheck
from rest_framework import exceptions
from .pulso import request_auth_pulso
from config.settings import get_env_variable
from django.conf import settings
import importlib

User = get_user_model()

class EmailAuthBackend(ModelBackend):
    """Allow users to log in with their email address"""

    def authenticate(self, request, username=None, password=None, **kwargs):
        # Some authenticators expect to authenticate by 'username'
        email = username
        if email is None:
            email = kwargs.get('username')

        try:
            user = User.objects.get(email=email)
            if user.check_password(password):
                user.backend = "%s.%s" % (self.__module__, self.__class__.__name__)
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

class PulsoAuthBackend(ModelBackend):
    """ Autoriza al ingreso de usuarios a través de Pulso """
    def authenticate(self, request, username=None, password=None, **kwargs):
        # Busca al usuario en la base de datos
        try:
            user = User.objects.get(username=username)

            if user.check_password(password):
                return user
            else:
                return None

        # Si el usuario no existe en la base de datos local
        # entonces lo busca en la base de datos de pulso
        except User.DoesNotExist:

            # busca la url de pulso en el archivo .env
            pulso_url = get_env_variable('URL_AUTH_PULSO')
            token, response = request_auth_pulso(pulso_url, username, password)

            # Si el usuario tampoco existe en pulso retorna None
            if token is None:
                return None

            # Si existe en pulso entonces crea el usuario en
            # la base de datos local
            else:
                user = User(username=username)
                user.set_password(password)
                user.save()
                # Ahora ejecutamos la función para guardar los datos de usuario
                current_module = importlib.import_module(settings.PULSO_HOOK_USER)
                current_module.register_new_user(response, user)
                print('REGISTRO NUEVO FINALIZADO')
                return user

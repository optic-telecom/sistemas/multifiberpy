import requests

from .permissions import IsSystemInternalPermission, TOKEN

def request_address_pulso(id):
    headers = {'Authorization': TOKEN}
    ####  Esta URL debe venir por configuraciónb Fred no en el código 
    r = requests.get(f'http://pulso.multifiber.cl/api/v1/complete-location/{id}/', headers=headers, timeout=5)
    status_code = r.status_code
    address = r.json() 
    return status_code, address


def get_complete_address(id):

    status_code, address = request_address_pulso(id)
    
    if status_code == 200:
        res = { 
            'ok': True,
            'data': {
                'id': address['id'],
                'number': address['number'],
                'country': address['country'],
                'country_id': address['country_id'],
                'region': address['region'],
                'region_id': address['region_id'],
                'commune': address['commune'],
                'commune_id': address['commune_id'],
                'street': address['street'],
                'street_id': address['street_id'],
                'tower': address['tower'],
                'floor': address['floor_num'],
                'department': address['departament'],
                'street_location': address['street_location']
                
            }
            
        }

        return res

    else :
        return {
            'ok': True, 
            'detail': f'error {status_code}', 
            'data': {
                'id': '-',
                'number': '-',
                'country': '-',
                'country_id': '-',
                'region': '-',
                'region_id': '-',
                'commune': '-',
                'commune_id': '-',
                'street': '-',
                'street_id': '-',
                'tower': '-',
                'floor': '-',
                'department': '-',
                'street_location': '-'
            }
        }
    

def request_auth_pulso(url, username, password):
    # crea el body del request
    body = {
        'username': username,
        'password': password
    }

    # hace el request a pulso
    request = requests.post(data=body, url=url)
    response = request.json()
    # verifica si el usuario fue autenticado
    if 'token' in response:
        return True, response

    elif 'non_field_errors' in response:
        return None, None

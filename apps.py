from django.apps import AppConfig


class MultifiberpyConfig(AppConfig):
    name = 'multifiberpy'

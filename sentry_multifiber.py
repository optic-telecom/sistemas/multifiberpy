import os
import requests
try:
    from django.conf import settings
    is_settings = True
except ImportError:
    is_settings = False


def send_slack_event(event, hint):
    fields = []

    # Define event variables

    # función que ocasionó el error
    function = event['exception']['values'][-1]['stacktrace']['frames'][-1]['function']

    # tipo de función, ej. ZeroDivisionError o TypeError
    function_type = event['exception']['values'][-1]['type']

    # valor del error
    value = event['exception']['values'][-1]['value']

    # Número de línea del error
    lineno = event['exception']['values'][-1]['stacktrace']['frames'][-1]['lineno']

    # archivo que ocasionó el error
    file = event['exception']['values'][-1]['stacktrace']['frames'][-1]['abs_path'] + ':' + str(lineno)

    # línea de código del archivo que ocasionó el error
    context_line = event['exception']['values'][-1]['stacktrace']['frames'][-1]['context_line']

    # 5 líneas de código antes de la línea de código que ocasiónó el error
    pre_context = event['exception']['values'][-1]['stacktrace']['frames'][-1]['pre_context']

    # 5 líneas de código luego de la línea de código que ocasiónó el error
    post_context = event['exception']['values'][-1]['stacktrace']['frames'][-1]['post_context']


    #Si un proyecto no quiere enviar errores
    if is_settings:
        if hasattr(settings, 'SKIP_SENTRY_ERRORS'):
            if value in settings.SKIP_SENTRY_ERRORS:
                return event


    # Se agrega la información a cada campo
    fields.append({
        "title": "Función",
        "value": function,
        "short": True
    })

    fields.append({
        "title": "Tipo",
        "value": function_type,
        "short": True
    })

    fields.append({
        "title": "Valor",
        "value": value,
        "short": True
    })

    fields.append({
        "title": "Archivo",
        "value": file,
        "short": False
    })

    """ Se itera sobre la lista de líneas para adjuntarlas en un string
        ya que el campo value solo acepta strings  """
    new_pre_context = ""
    count = 5
    for line in pre_context:
        if line != "":
            new_pre_context += line.strip() + f" <<< Línea: {lineno-count}"+"\n"
            count -= 1

    fields.append({
        "title": "Líneas antes del Error",
        "value": new_pre_context ,
        "short": False
    })


    fields.append({
        "title": "Línea de error",
        "value": context_line + f" <<< Línea: {lineno}",
        "short": False
    })


    """ Se itera sobre la lista de líneas para adjuntarlas en un string
        ya que el campo value solo acepta strings  """
    new_post_context = ""
    count = 1
    for line in post_context:
        if line != "":
            new_post_context += line.strip() + f" <<< Línea: {lineno+count}"+"\n"
            count += 1

    fields.append({
        "title": "Líneas luego del Error",
        "value": new_post_context ,
        "short": False
    })

    # Se crea el título del mensaje con el proyecto y el valor del error
    
    if is_settings:
        proyecto = 'Proyecto: ' + settings.SITE_NAME + " >> " + value + " <<"
        if hasattr(settings, 'ENVIROMENT'):
            proyecto = proyecto + ' - ' + settings.ENVIROMENT
        entorno = settings.ENVIROMENT.lower().strip()
    else:
        proyecto = 'Proyecto: ' + os.environ.get('SITE_NAME') + " >> " + value + " <<"
        if os.environ.get('ENVIRONMENT', None) != None:
            proyecto = proyecto + ' - ' + os.environ.get('ENVIRONMENT')
        entorno = os.environ.get('ENVIROMENT')
    
    data_slack = {
        "text": proyecto,
        "attachments": [
          {
            "color": "#c82d10",
            "text": event['level'],
            "fields": fields
          }       
        ]
    }


    entornos_desarrollo = ['development', 'dev', 'stage', 'desarrollo', ]

    if entorno in entornos_desarrollo:
        url_slack = os.environ.get(
            'URL_SENTRY_SLACK',
            'https://hooks.slack.com/services/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')
    else:
        url_slack = os.environ.get(
            'URL_SENTRY_SLACK',
            'https://hooks.slack.com/services/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')

    response_slack = requests.post(
        url=url_slack,
        json=data_slack
    )
    return event
